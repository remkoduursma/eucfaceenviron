# Quick environmental drivers at the EucFACE

Downloads data from the HIEv on rain, soil water content, and air temperature. 
Rain is measured at FACE (x 3) and the ROS (all four are included), soil water content at FACE, and air temperature at the ROS (because the timeseries is longer, since June 2011).

If you run the `eucface_environment_plot_run.R` script, it will :
- Download all necessary raw data into `cache/` (will take quite some time on the first run!)
- Produce daily dataframes, `faceraindaily`, `facesoilwater`, `airt`, each of which are self-explanatory.
- Make a PDF figure, written to `output/`. Start and end dates for the figure can be set in the script.

## Example figure:

![](https://bitbucket.org/remkoduursma/eucfaceenviron/raw/master/docs/example_figure.png)

