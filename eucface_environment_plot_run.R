
# Load.
source("functions.R")

library(HIEv)
setToken() # assume token is in c:/hiev/tokenfile.txt, or paste your token there.

# set path for HIEv
if(!dir.exists("cache"))dir.create("cache")
setToPath("cache")

if(!dir.exists("output"))dir.create("output")

if(!require(pacman))install.packages("pacman")
pacman::p_load(dplyr, doBy, mgcv, stringr)

#--- Get data from HIEv

# Rainfall
faceraindaily <- get_rain("daily")

# Soil water (>300MB)
facesoilwater <- get_soilwater()

# Air temperature from ROS
airt <- get_rosTair()



#--- Make plot
pdf(file="output/eucface_environ_timeseries.pdf",
    width=12, height=6)
par(mar=c(2,5,1,5))
figure_EucFACE_environment_timeseries(facesoilwater, faceraindaily, airt,
                                      start_date="2013-1-1", end_date="2018-1-1")
dev.off()





